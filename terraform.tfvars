subscription_id="841c2678-4875-4da5-a5ae-04aac4876ad4"
client_id="7a94bce3-f0ea-4472-8b7c-238b9571fcda"
client_secret="a6bd10ac-e025-4cdd-b33a-41831ad0e2e0"
tenant_id="c91572fa-c23b-4c84-ba59-e8dec14c6350"
name={
  "0"="virtualnetwork_1"
  "1"="virtualnetwork_2"
}
address_space="10.0.0.0/16"
name1={
  "0"="subnet_1"
  "1"="subnet_2"
}
name2="public_ip1"
name3={
  "0"="network_interface05"
  "1"="network_interface02"
  "2"="network_interface03"
  "3"="network_interface04"
}
name4={
  "0"="testconfiguration05"
  "1"="testconfiguration02"
  "2"="testconfiguration03"
  "3"="testconfiguration04"
}
name8={
  "0"="security_group_1"
  "1"="security_group_2"
}
name9={
  "0"="storagegtm1"
  "1"="storagegtm2"
}
name10={
  "0"="devops-app-05"
  "1"="devops-app-02"
  "2"="devops-app-03"
  "3"="devops-app-04"
}
name11={
  "0"="OsDisk_05"
  "1"="OsDisk_02"
  "2"="OsDisk_03"
  "3"="OsDisk_04"
}
name14={
  "0"="SSH"
  "1"="HTTP"
  "2"="HTTP_2"
}
name16={
  "0"="GTM_1"
  "1"="GTM_2"
}
location1={
  "0"="East US"
  "1"="East US2"
}
location2={
  "0"="East US"
  "1"="East US2"
}
adress1="10.0.1.0/24"
location3="East US2"
adress2="dynamic"
adress3="dynamic"
location5={
  "0"="East US"
  "1"="East US2"
}
account1="LRS"
account2="Standard"
location6={
  "0"="East US"
  "1"="East US2"
}
vm1="Standard_DS1_v2"
caching1="ReadWrite"
creat1= "FromImage"
managed1="Premium_LRS"
publi1="Canonical"
offer1="UbuntuServer"
sku1="16.04.0-LTS"
version="latest"
computer_name1="myVM1zuer834"
admin_user1="stage"
path1="/home/stage/.ssh/authorized_keys"
key_data1="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5G4vIkS5dGpeo2W/WWlfX2X3JEjM0p3EJC6LG57ZPJqZ1MBl0NWTTvs/HfoYW3GRCwSykjMj1pG8VpxFvEVRcIiz1G40JRDwK6MzLhtl952NQcB7OuNZzzOSAx1+PEgBkiMUxkr9ixcZ9Z4CVtP/aft3qs2FGr2cqzC2uOU2fpSCPLLCJqTHOAOPE3BqIC8UhS3OPGxQtwr/alpmQ/4HVryPbnSwwVG6yjBjCIW+/wF1C3gd+dCPGrRbBstJa2vwezs+/8qnWGDEpZxVPBNPgRC84plluJrf2YjzbOYXIpe2XAVPAD9MJOFw3zyJbhtIDtluU5o6zpF/F+mGttW81"
enable1="true"
vm2="Standard_DS1_v2"
caching2="ReadWrite"
creat2="FromImage"
managed2="Premium_LRS"
publi2="Canonical"
offer2="UbuntuServer"
sku2="16.04.0-LTS"
computer_name2="myVM2iewzr"
admin_username1="stage"
admin_password1="pA1!assword"
enabled2="true"
