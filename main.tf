resource "azurerm_resource_group" "newresourcegroup" {
  name     = "${var.name16["0"]}"
  location = "${var.location1["0"]}"
}
resource "azurerm_resource_group" "newresourcegroup2" {
  name     = "${var.name16["1"]}"
  location = "${var.location1["1"]}"
}
resource "azurerm_virtual_network" "newvirtualnetwork" {
   name                = "${var.name["0"]}"
   resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"
   address_space       = ["${var.address_space}"]
   location            = "${var.location2["0"]}"
}
resource "azurerm_virtual_network" "newvirtualnetwork2" {
   name                = "${var.name["1"]}"
   resource_group_name = "${azurerm_resource_group.newresourcegroup2.name}"
   address_space       = ["${var.address_space}"]
   location            = "${var.location2["1"]}"
}
resource "azurerm_subnet" "newsubnet" {
  name                 = "${var.name1["0"]}"
  resource_group_name  = "${azurerm_resource_group.newresourcegroup.name}"
  virtual_network_name = "${azurerm_virtual_network.newvirtualnetwork.name}"
  address_prefix       = "${var.adress1}"
}
resource "azurerm_subnet" "newsubnet2" {
  name                 = "${var.name1["1"]}"
  resource_group_name  = "${azurerm_resource_group.newresourcegroup2.name}"
  virtual_network_name = "${azurerm_virtual_network.newvirtualnetwork2.name}"
  address_prefix       = "${var.adress1}"
}
resource "azurerm_public_ip" "newpublicip" {
  name                         = "${var.name2}"
  location                     = "${var.location3}"
  resource_group_name          = "${azurerm_resource_group.newresourcegroup2.name}"
  public_ip_address_allocation = "${var.adress2}"
}
resource "azurerm_network_interface" "newnetworkinterface" {
  count               = "3"
  name                = "${lookup(var.name3, count.index + 1)}"
  location            = "${azurerm_resource_group.newresourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"
  ip_configuration {
    name                          = "${lookup(var.name4, count.index + 1)}"
    subnet_id                     = "${azurerm_subnet.newsubnet.id}"
    private_ip_address_allocation = "${var.adress3}"
  }
}
resource "azurerm_network_interface" "newnetworkinterface2" {
  name                = "${var.name3["0"]}"
  location            = "${azurerm_resource_group.newresourcegroup2.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup2.name}"
  ip_configuration {
    name                          = "${var.name4["0"]}"
    subnet_id                     = "${azurerm_subnet.newsubnet2.id}"
    private_ip_address_allocation = "${var.adress3}"
    public_ip_address_id          = "${azurerm_public_ip.newpublicip.id}"
  }
}
resource "azurerm_network_security_group" "newnetworksecuritygroup" {
  name                = "${var.name8["0"]}"
  location            = "${azurerm_resource_group.newresourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"
  security_rule {
    name                       = "${var.name14["0"]}"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name14["1"]}"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_security_group" "newnetworksecuritygroup2" {
  name                = "${var.name8["1"]}"
  location            = "${azurerm_resource_group.newresourcegroup2.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup2.name}"
  security_rule {
    name                       = "${var.name14["0"]}"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name14["1"]}"
    priority                   = 1004
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name14["2"]}"
    priority                   = 1005
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.newresourcegroup.name}"
    }
    byte_length = 8
}
resource "random_id" "randomId2" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = "${azurerm_resource_group.newresourcegroup2.name}"
  }
  byte_length = 8
}
resource "azurerm_storage_account" "newstorageaccountgtm" {
  name                     = "${var.name9["0"]}"
  resource_group_name      = "${azurerm_resource_group.newresourcegroup.name}"
  location                 = "${var.location5["0"]}"
  account_replication_type = "${var.account1}"
  account_tier             = "${var.account2}"
}
resource "azurerm_storage_account" "newstorageaccountgtm2" {
  name                     = "${var.name9["1"]}"
  resource_group_name      = "${azurerm_resource_group.newresourcegroup2.name}"
  location                 = "${var.location5["1"]}"
  account_replication_type = "${var.account1}"
  account_tier             = "${var.account2}"
}
resource "azurerm_virtual_machine" "newVM1HELP" {
  count                 = "3"
  name                  = "${lookup(var.name10, count.index + 1)}"
  location              = "${var.location6["0"]}"
  resource_group_name   = "${azurerm_resource_group.newresourcegroup.name}"
  network_interface_ids = ["${element(azurerm_network_interface.newnetworkinterface.*.id, count.index)}"]
  vm_size               = "${var.vm1}"
  storage_os_disk {
    name              = "${lookup(var.name11, count.index + 1)}"
    caching           = "${var.caching1}"
    create_option     = "${var.creat1}"
    managed_disk_type = "${var.managed1}"
  }
  storage_image_reference {
    publisher = "${var.publi1}"
    offer     = "${var.offer1}"
    sku       = "${var.sku1}"
    version   = "${var.version}"
  }
  os_profile {
    computer_name  = "${var.computer_name1}"
    admin_username = "${var.admin_user1}"
  }
  os_profile_linux_config {
    disable_password_authentication = true
      ssh_keys {
        path     = "${var.path1}"
        key_data = "${var.key_data1}"
      }
  }
  boot_diagnostics {
    enabled     = "${var.enable1}"
    storage_uri = "${azurerm_storage_account.newstorageaccountgtm.primary_blob_endpoint}"
    }
}
resource "azurerm_virtual_machine" "newVM2HELP" {
  name                   = "${var.name10["0"]}"
  location               = "${var.location6["1"]}"
  resource_group_name    = "${azurerm_resource_group.newresourcegroup2.name}"
  network_interface_ids  = ["${azurerm_network_interface.newnetworkinterface2.id}"]
  vm_size                = "${var.vm2}"
  storage_os_disk {
    name                = "${var.name11["0"]}"
    caching             = "${var.caching2}"
    create_option       = "${var.creat2}"
    managed_disk_type   = "${var.managed2}"
  }
  storage_image_reference {
    publisher = "${var.publi2}"
    offer = "${var.offer2}"
    sku = "${var.sku2}"
    version = "${var.version}"
  }
  os_profile {
    computer_name = "${var.computer_name1}"
    admin_username = "${var.admin_username1}"
    admin_password = "${var.admin_password1}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  boot_diagnostics {
    enabled = "${var.enabled2}"
    storage_uri = "${azurerm_storage_account.newstorageaccountgtm2.primary_blob_endpoint}"
  }
}